module "techit_streaming_eks_kafka_cluster_module" {
    source = "../techit-streaming-infra-modules/techit-streaming-eks"

    env                     = "sandbox"
    eks_cluster_name        =  var.eks_cluster_name
    eks_cluster_version     =  "1.25"
    eks_cluster_type        =  "ext"
    privatesubnets          =  
    VPCID                   = var.VPCID

    patch_zk_nodes          =   false
    patch_kafka_nodes       =   false
    patch_worker_nodes      =   false

    shared_workers_desired_size = 8
    kafka_desired_size          = 6
    infra_nodes_desired_size    = 3

    ebs_csi_addon_enabled   =   true
    set_kafka_lt_default    =   true

    #########launch template for EKS cluster################
    launch-template-kafka           =   "techit-streaming-launchtemplate-kafka-sbox"
    launch-template-zookeeper       =   "techit-streaming-launchtemplate-zookeeper-sbox"
    launch-template-shared          =   "shared-nodes-ext-sandbox"
    launch-template-infra           =   "ingress-nodes-ext-sbox"
    launch-template-k2              =   "techit-streaming-launch-template-k2"
    launch-template-cc              =   "techit-streaming-launch-template-cc"
    launch-template-awsconnect      =   "techit-streaming-launchtemplate-awsconnect"
    launch-template-onpremconnect   =   "techit-streaming-launchtemplate-onpremconnect"
    launch-template-srrp            =   "techit-streaming-launch-template-srrp"
    launch-template-meconnect       =   "techit-streaming-launchtemplate-meconnect"
    launch-template-rp              =   "techit-streaming-launchtemplate-rp"

    ########## EKS cluster SG ############
    eks-cluster-sg  =   "techit-streaming-eks-cluster-sg-sbox"
    eks-kafka-sg    =   "techit-streaming-eks-kafka-sg-sbox"
    eks-ingress-sg  =   "ingress-sg-ext-sbox"

    ############ IAM roles for EKS cluster #########
    eks-worker-nodes-iam-role   =   "techit-streaming-eks-worker-nodes-iam-role-sbox"

    ############ worker node groups #########
    zk_node_group       =   "techit-streaming-worker-nodes-zookeeper-sbox"
    kafka_node_group    =   "techit-streaming-worker-nodes-kafka-sbox"
    cc_subnet_id        =   [""]
    rp_subnet_id        =   [""]

    administrator_role_arn          = ""
    cluster_management_role_name    = "techitclustermanagement"

}