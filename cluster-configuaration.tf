module "cluster-configuaration" {
  source                = "../techit-streaming-inframodules/cluster-configuaration"
  eks_cluster_name      = var.eks_cluster_name
  aws_cni_env_variables = {
    set                 = false
    warm_ip_target      = "1"
    minimum_ip_target   = "1"
  }
}