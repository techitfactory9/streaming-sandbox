resource "aws_security_group_rule" "techit_streaming_eks_cluster_sg_rule1" {
    from_port = 443
    description = "NAT Gateway IP address"
    protocol = "tcp"
    security_group_id = module.techit_streaming_eks_kafka_cluster_module.techit_streaming_eks_cluster_sg
    to_port =   443
    type = "ingress"
    cidr_blocks = ["13.48.228.248/32", "13.51.76.45/32", "13.51.129.156/32"]   
        }

resource "aws_security_group_rule" "techit_streaming_eks_cluster_sg_rule5" {
    from_port = 9092
    description = "SAP CPI IP Address"
    protocol = "tcp"
    security_group_id = module.techit_streaming_eks_kafka_cluster_module.techit_streaming_eks_cluster_sg
    to_port =   9092
    type = "ingress"
    cidr_blocks = ["3.68.48.83/32", "18.198.18.157/32", "3.68.17.221/32", "3.67.235.98/32", "3.68.38.23/32","18.198.149.19/32", "3.64.88.217/32","3.64.142.24/32"]   
        }
resource "aws_security_group_rule" "techit_streaming_eks_cluster_sg_rule7" {
    from_port = 30000
    description = " techit Team Client IP"
    protocol = "tcp"
    security_group_id = module.techit_streaming_eks_kafka_cluster_module.techit_streaming_eks_cluster_sg
    to_port =   32767
    type = "ingress"
    cidr_blocks = ["139.122.191.8/24","138.186.57.131/32"]   
        }