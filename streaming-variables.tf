# Variable definitions here 

variable "region" {
    description = "AWS region where we place resource"
    default = "eu-north-1"
  
}

# Assume this role to deploy. can be cross-account to deploy in other accounts!

variable "assume_role" {
    description = "AWS IAM rle ARN to assume"
  
}

##################################################################
The settings below are automatically extracted from the Gitlab environment
######################################################################

variable "VPCID" {
    description = "VPC ID of the account"
  
}

variable "VPC_CIDR_BLOCK" {
    default = "11.178.88.8/22"
  
}

variable "eks_cluster_name" {
    type = string
  
}

variable "env"{
    type = string
    default = "sandbox"
}
