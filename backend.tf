terraform {
  backend "s3" {
    region = "eu-north-1"
  }
  required_providers {
    aws = {
        source = "hashicorp/aws"
        version = ">=3.74"
    }
  }
}